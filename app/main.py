from fastapi import FastAPI

app = FastAPI()

@app.get("/health")
async def read_health():
    return {"status": "ok"}

@app.get("/hello/{name}")
def read_item(name: str):
    output_name = predict_from_ml_model(data=name)
    return {"message": f"Predicted: {output_name}"}

def predict_from_ml_model(data: str):
    return data


